
public class Main {
	public static void main(String[] args) {
    	final User magdin = new User("Magdin");
    	final User ed = new User("Ed");
        
    	Thread t1 = new Thread("t1") {
    		public void run() {
    			System.out.println("Doing: "+ this.getName());
    			magdin.friend(ed, this.getName());
    			magdin.defriend(ed, this.getName());
    			System.out.println(this.getName() + " done!");
    		}
    	};
    	
    	Thread t2 = new Thread("t2") {
    		public void run() {
    			System.out.println("Doing: "+ this.getName());
    			ed.friend(magdin, this.getName());
    			ed.defriend(magdin, this.getName());
    			System.out.println(this.getName() + " done!");
    		}
    	};
    	
    	t1.start();
    	t2.start();
    }
}
