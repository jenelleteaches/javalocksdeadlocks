import java.util.HashSet;
import java.util.Set;

public class User {
    private final String name;
    private final Set<User> friendsList;
    
    // Add an explicit lock - this is the same as installing a door on the bathroom stall
    private final Object lock = new Object();
    
    public User(String name) {
        this.name = name;
        this.friendsList = new HashSet<User>();
    }

    public boolean isFriendsWith(User u2) {
        return this.friendsList.contains(u2);
    }

    // Note: Removed the synchronization
    public void friend(User u2, String threadName) {
        synchronized (lock) {
        	boolean friendAdded = this.friendsList.add(u2);
            if (friendAdded) {
            	u2.friend(this, threadName);
            	System.out.println(this.name + " friended " + u2.name);
            } 
        }
    }
    
    public void defriend(User u2, String threadName) {
		boolean friendRemoved = this.friendsList.remove(u2);
	    if (friendRemoved) {
	        u2.defriend(this, threadName);
	        System.out.println(this.name + " defriended " + u2.name);
	    }   
    }
   
}


